##########
# Copyright 2024, Guilhem Mureau, Alice Pellet-Mary, Heorhii Pliatsok,
# Alexandre Wallet

# ModLipAssumption is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.  ModLipAssumption is
# distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero Public License
# for more details.  You should have received a copy of the GNU Affero
# Public License along with ModLipAssumption. If not, see
# <https://www.gnu.org/licenses/>

##########


############################################
##  This file contains all the functions  ##
##  needed to perform the  tests from     ##
##  file test_assumption.sage, as well    ##
##  as the functions to post-process the  ##
##  data in post-processing.sage          ##
############################################


####################
## Imports needed ##
## and settings   ##
####################
import time, csv
from sage.stats.distributions.discrete_gaussian_integer import DiscreteGaussianDistributionIntegerSampler
proof.arithmetic(False) ## if False: no proof for Sage's arithmetic computation
#precision = 100 ## used when rounding the real lattice to a rational lattice (denominators are 2^precision)

########################################
## Defining the number field          ##
## and computing its ring of integers ##
########################################

def create_field_objects(m, prec = 100, verbose = True):
  ## Input: m, the conductor of the cyclotomic field
  ## Output: a tuple (K, OK, d, B_OK, rho_K) where
  ##         K is the totally real subfield of the cyclotomic field of conductor m
  ##         OK is the ring of integers of K
  ##         d is the degree of K
  ##         B_OK is an LLL-reduced basis of OK
  ##         rho_K is the residue of the zeta function of K at 1
  global everything_went_well

  L = CyclotomicField(m)
  K = L.maximal_totally_real_subfield()[0]
  d = K.degree()
  if verbose:
    print("d = ", d)
  OK = K.ring_of_integers()
  B_OK = OK.basis()
  if verbose:
    print("OK computed")
  
  ## LLL reducing the OK basis to make further computation faster
  B_OK_sigma = Matrix(Reals(prec),[x.complex_embeddings(prec = 50+prec) for x in B_OK])
  B_OK_sigma_int = Matrix(ZZ,[[round(vi*2^prec) for vi in v] for v in B_OK_sigma])
  (_,U) = B_OK_sigma_int.LLL(transformation = True)
  B_OK_red = [sum(U[i,j]*B_OK[j] for j in range(d)) for i in range(d)]
  B_OK = B_OK_red
  if verbose:
    print("OK has been LLL reduced\n")

  ## Computing the residue of the zeta function at 1
  f = K.zeta_function()
  def g(x): return f(x)*(x-1) ## the residue is the limit of g at 1
  rho_K = g(1.00001) ## approximating the limit with an evaluation almost at one
  ## Testing numerical stability by comparing with g(1.001)
  rho_K_bis = g(1.001)
  if abs(rho_K-rho_K_bis)/rho_K > 0.1:
    everything_went_well = False
    print("\nCaution, the computation of the residue of the zeta function might be wrong")
    print("value of zeta_K(x)*(x-1) at x = 1.001: ", rho_K_bis)
    print("value of zeta_K(x)*(x-1) at x = 1.00001: ", rho_K)
    print("I am continuing with the second value\n")
  if verbose:
    print("rho_K computed: ", rho_K, "\n")
    
  return {'K':K, 'OK':OK, 'd':d, 'B_OK':B_OK, 'rho_K':rho_K, 'precision':prec}
  
##########################
## Auxilliary functions ##
##########################

def real_to_rational(x,prec):
  ## round a real number x to a rational number y, with at least prec bits of precision
  y = ZZ(round(x*2^prec))/2^prec
  return y
  
def close_values(x,y):
  ## Input: two real numbers
  ## Output: True if x and y are close (absolutely or relatively) and False otherwise
  if abs(x-y) < 0.01:
    return True
  if x != 0 and y != 0 and abs(x-y)/abs(x) < 0.05:
    return True
  return False

def module_to_lattice(B, B_OK, prec):
  ## Input: B a basis of a (free) module, with coefficients in K
  ## Output: B_lat, a basis of the lattice associated to B in canonical embedding
  ##         for technical purposes (due to the Gaussian sampler we use), we
  ##         round the coordinates of B_lat to make them rational 
  ##         (note that B_lat is in RR by default, because K is totally real)
  ##         this changes slightly the geometry of the lattice, but should not be significative
  B_lat_vec = []
  for i in range(B.nrows()):
    for alpha in B_OK:
      tmp_vec = []
      for j in range(B.ncols()):
        tmp_vec += list((alpha*B[i,j]).complex_embeddings(50+prec))
      tmp_vec_rounded = [real_to_rational(Reals(prec)(x),prec) for x in tmp_vec]
      B_lat_vec += [tmp_vec_rounded]
  return Matrix(QQ, B_lat_vec)
  
def lat_vec_to_mod_vec(v,B_lat_inv,B, field_objects):
  ## Input: a basis B of a (free) module with coefficient in K
  ##        B_lat_inv is the inverse of B_lat, which is is the basis of the lattice 
  ##        corresponding to B in RR (output by module_to_lattice(B))
  ##        v is a vector of the lattice spanned by B_lat (coefficients in RR)
  ## Output: a vector w with coeff in K, whose canonical embedding is v
  
  global everything_went_well
  d = field_objects['d']
  B_OK = field_objects['B_OK']
  K = field_objects['K']
  prec = field_objects['precision']
  
  B = Matrix(K,B) ## for some reasons, Sage cannot handle vectors over OK whenever the degree of K is too big, but vectors over K are fine
  coord_v = v*B_lat_inv ## coordinates of v in the basis B_lat
  in_span = True
  for i in range(len(coord_v)):
    if abs(coord_v[i]-round(coord_v[i])) > 0.01:
      in_span = False
    coord_v[i] = round(coord_v[i])
  coord_v = vector(ZZ, coord_v)
  if not in_span:
    everything_went_well = False
    print("\nWarning in lat_vec_to_mod_vec: vector v does not seem to belong to the lattice spanned by B_lat")
    print("Before rounding the coordinates we had: ", v*B_lat_inv,"\n")
  w = vector(K, B.ncols())
  for k in range(B.ncols()):
    w += sum(coord_v[d*k+i]*B_OK[i]*B[k] for i in range(d))
    
  ## Checking that everything went good (and v is the canonical embedding of w)
  v_bis = []
  for w_i in w:
    v_bis += list(w_i.complex_embeddings(50+prec))
  correct = True
  for i in range(len(v_bis)):
    if close_values(v[i], v_bis[i]) == False:
      correct = False
  if not correct:
    everything_went_well = False
    print("\nWarning in lat_vec_to_mod_vec: it seems that w does not correspond to v")
    print("v = ", v)
    print("canonical embedding of w = ", v_bis, "\n")
  return w
  
def create_OK_sampler(field_objects, verbose = True):
  ## Output: D_OK, a Gaussian sampler in OK, with minimal standard deviation sigma_B
  OK = field_objects['OK']
  B_OK = field_objects['B_OK']
  K = field_objects['K']
  prec = field_objects['precision']
  
  In = identity_matrix(OK, 1)
  B_L_OK = module_to_lattice(In, B_OK, prec)
  B_L_OK_inv = B_L_OK^(-1)
  sigma_B = RR(sqrt(log(2*K.degree()+4)/pi)*max([v.norm(2) for v in B_L_OK])) ## smallest possible sigma
  if verbose:
    print("\nsigma_B = ", Reals(20)(sigma_B), "\n")
  (Q,R) = GPV_init(B_L_OK)
  def D_OK():
    v = GPV(B_L_OK,Q,R, sigma_B)
    return lat_vec_to_mod_vec(v,B_L_OK_inv,In, field_objects)[0]
  return D_OK
  
def random_module(D_OK, dim, field_objects):
  ## Input: D_OK, a distribution over OK; dim an integer
  ## Output: a random square basis of a free module of rank dim
  ##         each coefficient of the square matrix is sampled according to
  ##         a discrete Gaussian distribution of parameter sigma in O_K
  ##         and we reject until the matrix has full rank over K
  
  K = field_objects['K']
  OK = field_objects['OK']
  
  ## Sampling a random matrix until it has full rank
  full_rank = False
  while not full_rank:
    B = matrix(OK, dim)
    for i in range(dim):
      for j in range(dim):
        B[i,j] = D_OK()
    if K(det(B)) != K(0):
      full_rank = True
      
  return B
  
  
def Gram_ideal(B, OK):
  ## Input: a basis B of a module M with coefficients in OK
  ## Output: the ideal generated by the square norms of the vectors of M
  ##         in our case where B is a basis of M, this is the ideal generated
  ##         by the coefficients of the Gram matrix B*B^T, multiplied by 2 for the non-diagonal ones
  G = B*B.transpose()
  I = OK.fractional_ideal(G[0,0])
  for i in range(G.ncols()):
    for j in range(G.ncols()):
      if i == j:
        I = I + OK.fractional_ideal(G[i,j])
      elif G[i,j] != OK(0): ## fractional_ideal does not like being a zero ideal
        I = I + OK.fractional_ideal(2*G[i,j])
  return I
      
  
########################################
## The discrete Gaussian sampler over ##
## a lattice in Sage seems broken     ##
## (e.g., try it on basis [1,5],[0,1] ##
## This should be a discrete Gaussian ##
## over ZZ^2, but instead the second  ##
## coordinate is always much larger   ##
## than the first one)                ##
########################################

########################################
## Reimplementing GPV's algorithm     ##
########################################

def GPV_init(B_lat):
  (Q,R) = Matrix(RDF,B_lat.transpose()).QR()
  Q = Matrix(RR,Q)
  R = Matrix(RR,R)
  for i in range(R.ncols()):
    if R[i,i] < 0: ## The function does not always output r_ii > 0
      for j in range(R.ncols()):
        Q[j,i] = - Q[j,i]
        R[i,j] = -R[i,j]
  R = R.transpose()
  Q = Q.transpose()
  return (Q,R)
  
def GPV(B_lat,Q,R, sigma):
  dim = R.ncols()
  c = vector(RR,dim)
  res = vector(RR,dim)
  
  for i in range(dim-1,-1,-1):
    c_tmp = c[i]/R[i,i]
    sigma_tmp = sigma/R[i,i]
    D = DiscreteGaussianDistributionIntegerSampler(sigma=sigma_tmp, c = c_tmp)
    z = D()
    c = c-z*R[i]
    res = res+z*B_lat[i]
  return res
  


######################################
####                              ####
####       Test functions         ####
####                              ####
######################################
    
  
##################################
## Testing for one fixed module ##
##################################


def test_fix_module(nb_tests, Chi2, field_objects, is_coprime_Gram = True, I_B = 'NULL', verbose = True):
  ## Input: nb_tests -> an integer, counting the number of random vectors sampled for the test
  ##        Chi2 -> A function outputting a random element of OK (here, the square norm of a Gaussian vector)
  ##        is_coprime_Gram -> True or False, if False, then I_B must be an ideal of OK dividing all elements output by Chi2
  ##                           in this case, the ideal J computed will be the output of Chi2 divided by I_B
  ##                           if True, then the algorithm behaves as if I_B = OK (and in this case input I_B will be ignored)
  ## Output: (x,y) -> x is an integer, corresponding to the number of elements q output by Chi2 such that
  ##                  q OK / I_B is a prime ideal (with I_B = OK if is_coprime_Gram = True)
  ##                  y is a real number, corresponding to the expectated probability that q*OK/I_B is prime
  ##                  for a given q. This expectated probability is estimated by 1/(rho_K*log(Norm(q*OK/I_B)))
  
  OK = field_objects['OK']
  rho_K = field_objects['rho_K']
  
  total_prime = 0
  expected_total_prime = 0 ## Expected number of prime elements to be found,
                           ## if the probability is 1/(rho_K*log(norm))
  if is_coprime_Gram:
    norm_IB = 1
  else:
    norm_IB = I_B.norm()
  for test in range(nb_tests):
    #print("1")
    if verbose and test%100 == 0:
      print(test, "tests done out of", nb_tests_per_module)
    q = OK(Chi2())
    #print("2")
    if q != OK(0):
      ## do not define J = q/I_B yet, because it's slow and we can avoid defining it most of the time
      norm_J = ZZ(norm(q)/norm_IB)
      if norm_J.is_prime_power(): #the first if is to avoid costly primality tests
        if is_coprime_Gram:
          J = OK.fractional_ideal(q)
        else:
          J = OK.fractional_ideal(q)/I_B ## can be very slow!
        if J.is_prime():
          total_prime += 1
      if norm_J != 1: ## in small dimensions, it happens sometimes that J = OK, in which case the value below is infinite
        expected_total_prime += RR(1/(rho_K*log(norm_J)))
    
  if verbose:
    print("\nTotal number of prime elements found : \n ", total_prime)
    print("\nExpected number of prime elements, if principal ideals behave as abritrary ideals: \n", expected_total_prime)
  return(total_prime, expected_total_prime)
  
################################
## Testing for random modules ##
################################


def test_random_module(nb_modules, nb_tests, field_objects, type_module = 'None', verbose = True):
  ## Input : nb_modules -> an integer, corresponding to the number of random modules to generate
  ##         nb_tests -> an integers, number of random vectors sampled per module
  ##         field_objects -> a dictionnary which is the output of the function `create_fields_objects' (contains K, OK, d, ...)
  ##         type_module -> one of the three values 'None', 'Coprime', 'OK2'
  ## Output : a list of nb_modules triples (x,y,sigma'), where each triple correspond to a random module M, sigma is
  ##          the standard deviation used for the Gaussian distribution in M divided by N(I)^{1/d}, where I is the Gram ideal of M
  ##          and (x,y) is the output of test_fix_module on M        
  ##          if type_module = 'None', then the modules are sampled randomly, without restriction
  ##          if type_module = 'Coprime', then the modules are sampled randomly until their Gram ideal is OK
  ##          if type_module = 'OK2', then the module is OK^2, and the integer nb_modules is ignored (only 1 test is done)

  res = []
  global everything_went_well
  (OK, B_OK, K, prec) = (field_objects['OK'], field_objects['B_OK'], field_objects['K'], field_objects['precision'])
  
  ## Testing for possible typos
  if not (type_module in ['None', 'Coprime', 'OK2']):
    everything_went_well = False
    print("Bad input in test_random_module")
    return -1
  
  ## putting nb_modules = 1 in the case 'OK2'
  if type_module == 'OK2':
    nb_modules = 1
  
  ## set up the discrete Gaussian sampler over OK in the case 'None' and 'Coprime'
  else:
    D_OK = create_OK_sampler(field_objects, verbose = verbose)
  
  ##
  ## iteration over each random module
  ##
  for n_mod in range(nb_modules):
    if n_mod%5 == 0 and not type_module == 'OK2':
      print(n_mod, "modules tested out of", nb_modules)
      
    ## generating the random module M
    if type_module == 'OK2':
      B = identity_matrix(OK,2)
    elif type_module == 'None':
      B = random_module(D_OK, 2, field_objects)
      I_B = Gram_ideal(B, OK)
      is_coprime_Gram = False ## will be used when calling test_fix_module to make it faster
      if I_B == OK.fractional_ideal(1):
        is_coprime_Gram = True 
    else:
      B = random_module(D_OK, 2, field_objects)
      I_B = Gram_ideal(B, OK)
      while I_B != OK.fractional_ideal(1):
        B = random_module(D_OK, 2, field_objects)
        I_B = Gram_ideal(B, OK)

    if verbose:
      print("random module B has been computed")
      if type_module == 'None':
        print("I_B = ", I_B)
      
    ## define the discrete Gaussian distribution over the module lattice
    ## spanned by B
    B_lat = module_to_lattice(B, B_OK, prec)
    B_lat_red = B_lat.LLL() ## we LLL reduce the lattice basis, so that we can sample smaller elements inside
    B_lat_inv = B_lat^(-1)
    (Q,R) = GPV_init(B_lat_red) ## uses the LLL reduced basis to sample from Gaussian distribution with smaller sigma
    sigma = RR(sqrt(log(2*K.degree()+4)/pi)*max([R[i,i] for i in range(R.ncols())])) ## smallest possible sigma
    if type_module == 'OK2':
      sigma = 2*sigma ## increase sigma for OK2, it remains small and so reasonnable
    if verbose:
      print("sigma = ", Reals(20)(sigma))
    ## The distribution we want: sample a Gaussian vector in the module M spanned by B
    ## and output the sum of the square of its coordinates (in O_K)
    def Chi2():
      v = GPV(B_lat_red,Q,R, sigma)
      w = lat_vec_to_mod_vec(v,B_lat_inv,B, field_objects) ## w in M is Gaussian
      return sum(w_i^2 for w_i in w)
      
    ## Doing the test for fixed module
    if type_module == 'None':
      (total_prime, expected_prime) = test_fix_module(nb_tests, Chi2, field_objects, is_coprime_Gram = is_coprime_Gram, I_B = I_B, verbose = False)
      sigma_bis = sigma/RR(norm(I_B))^(1/K.degree())
      res += [(total_prime, Reals(20)(expected_prime), sigma_bis )]
    else:
      (total_prime, expected_prime) = test_fix_module(nb_tests, Chi2, field_objects, verbose = False)
      res += [(total_prime, Reals(20)(expected_prime), sigma)]
  return res
  
#################################
## Main function running the   ##
## tests for a given conductor ##
## and printing the resulting  ##
## data in a file              ##
#################################

def generate_data(m,random_modules,fixed_random_seed = True, verbose = False):
  ## Input: m an integer which is the conductor of the cyclotomic field to test
  ##        random_modules is a boolean, which decides whether we run the test on randomly generated modules or on OK^2
  #         fixed_random_seed is a boolean value, if set to True, the random seed will be fixed to 42, otherwise it is not fixed
  ## Output: the output is written in a csv file (with a format described in the readme) whose name is `results/output_OK_m.csv'
  ##         or `results/output_module_m.csv', depending whether random_modules is False or True, and where where m is replaced 
  ##         by the actual value of the input m (if such a file already exists, it is erased)
  
  ## Fixing the random seed for reproducibility
  if fixed_random_seed:
    set_random_seed(42)
  
  d = euler_phi(m)/2
  if verbose:
    print("m = ", m)
    print("d = ", d)

  ## case with 20 randomly generated modules
  if random_modules:
    ## parameters
    if d < 21:
      nb_tests = 2000
      nb_modules = 20
    else:
      nb_tests = 1000
      nb_modules = 10
    if verbose:
      print("nb_tests = ", nb_tests)
      print("nb_modules = ", nb_modules)

    ## generating field
    t1 = time.time()
    field_objects = create_field_objects(m, verbose = False)
    t2 = time.time()
    if verbose:
      print("\nK and other field related parameters have been generated in time:", Reals(20)(t2-t1),"seconds")

    ## Running the test
    if verbose:
      print("\nStarting the test for", nb_modules, "random modules...")
    res_random_mod = test_random_module(nb_modules, nb_tests, field_objects, verbose = False)
    if verbose:
      print("...done")
    t3 = time.time()
    if verbose:
      print("Time needed for the random modules test: ", t3-t2," seconds\n")
      
    ## Writting the output
    output_file = 'results/output_module_'+str(m)+'.csv'
    with open(output_file, 'w', newline='') as csvfile:
      fieldnames = ['m', 'd', 'nb_tests', 'rho_K', 'everything_went_well', 'P_empirical', 'P_expected', 's']
      writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

      writer.writeheader()
      for l in res_random_mod:
        writer.writerow({'m': m, 'd': field_objects['d'], 'nb_tests': nb_tests, 'rho_K': field_objects['rho_K'], 'everything_went_well': everything_went_well, 'P_empirical': l[0], 'P_expected': l[1], 's': l[2]})
      
  ## case where the module is OK^2 (only 1 module)
  else:
    ## parameters
    nb_tests = 5000
    nb_modules = 1
    precision = 100  
    
    if d > 92:
      precision = 150  
    if d > 100:
      precision = 250
    if d > 120:
      precision = 350
      nb_tests = 3000
      pari.allocatemem(s = pari.stacksize(), sizemax = 2*2^30)
    if verbose:
      print("nb_tests = ", nb_tests)
      print("nb_modules = ", nb_modules)

    ## generating field
    t1 = time.time()
    field_objects = create_field_objects(m, prec = precision, verbose = False)
    t2 = time.time()
    if verbose:
      print("\nK and other field related parameters have been generated in time:", Reals(20)(t2-t1),"seconds")

    ## Running the test
    if verbose:
      print("\nStarting the test for OK^2...")
    res_random_mod = test_random_module(nb_modules, nb_tests, field_objects, type_module = 'OK2', verbose = False)
    if verbose:
      print("...done")
    t3 = time.time()
    if verbose:
      print("Time needed for the test: ", Reals(20)(t3-t2)," seconds\n")
      
    ## Writting the output
    output_file = 'results/output_OK_'+str(m)+'.csv'
    with open(output_file, 'w', newline='') as csvfile:
      fieldnames = ['m', 'd', 'nb_tests', 'rho_K', 'everything_went_well', 'P_empirical', 'P_expected', 's']
      writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

      writer.writeheader()
      for l in res_random_mod:
        writer.writerow({'m': m, 'd': field_objects['d'], 'nb_tests': nb_tests, 'rho_K': field_objects['rho_K'], 'everything_went_well': everything_went_well, 'P_empirical': l[0], 'P_expected': l[1], 's': l[2]})
  return(1)
  
##################################
## Functions to process the     ##
## data obtained from the tests ##
##################################
  
def relative_error(list_triples):
  ## Input: list_triples a list of triples of real numbers, with the second one always non-zero
  ## Output: the list of quotients between the first and the second values of each triple
  return [RR(x/y) for (x,y,_) in list_triples]
  
def estimate_polynomial(list_triples, nb_tests, rho_K):
  ## Input: list_triples as output by the function test_random_module
  ##        nb_tests the number of random vectors sampled for each module
  ## Output: the quantity 1/(empirical_proba*rho_K*log(sigma/norm(I)^{1/d})), which is supposed to be P(d)
  ##         according to our assumption, for some polynomial P
  res = []
  for (x,y,sigma_bis) in list_triples:
    empirical_proba = RR(x/nb_tests)
    log_sigma = max(1, log(sigma_bis)) ## when sigma_bis is too small, 1/log(sigma) may become very large or even negative
                                       ## this gives incoherent values, and this is not the regime in which we will be
                                       ## interested for sigma anyway
    poly = 1/(empirical_proba*rho_K*log_sigma)
    if poly < 0:
      poly = 0 
    res += [poly]
  return res
  
####################################
## Functions to print the results ##
####################################

list_colors = list(colors.keys())

def print_results(res):
  ## Input: the list res_modules or res_OK
  ## Output: two graphical objects, 
  ##         one displaying the relative error between the prime probability and the expected prime probability
  ##         one displaying the quantity P(d) as a function of d (from the assumption in the article)
  g_relative = Graphics()
  g_poly = Graphics()
  for z in res:
    (m,d,nb_tests, rho_K, everything_went_fine, l) = z
    if everything_went_fine: ## Only print points for which no warning occured
      relative = relative_error(l)
      poly = estimate_polynomial(l, nb_tests, rho_K)
      color = list_colors[ZZ.random_element(len(list_colors))]
      for t in relative:
        g_relative += point((d, RR(t)), color = color)
      for t in poly:
        g_poly += point((d, RR(t)), color = color)
  return (g_relative, g_poly)

def OK_among_modules(g_OK, g_modules):
  ## Input: two graphical objects made of colored points
  ## Output: a new graphical object where the points of g_modules are blue
  ##         and the points of g_OK are red
  g_sum = Graphics()
  for p in g_modules:
    g_sum += point(p, color = 'blue')
  for p in g_OK:
    g_sum += point(p, color = 'red', size = 30, marker = "x")
  return g_sum
  
