##########
# Copyright 2024, Guilhem Mureau, Alice Pellet-Mary, Heorhii Pliatsok,
# Alexandre Wallet

# ModLipAssumption is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.  ModLipAssumption is
# distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero Public License
# for more details.  You should have received a copy of the GNU Affero
# Public License along with ModLipAssumption. If not, see
# <https://www.gnu.org/licenses/>

##########

#######################################################################################
## This sage file runs the experiments on various inputs, in order to verify         ##
## the Assumption 1 of https://eprint.iacr.org/2024/441.pdf                          ##
## It produces the data and the figures that are provided in the article             ##
##                                                                                   ##
## What you may want to modify:                                                      ##
## - nb_cpus (below, default is 3): the number of cpus used for parallel computation ##
##   you may reduce this number if you do not have enough computational power, or    ##
##   increase it if you want to run the computation faster                           ##
## - list_inputs: this is a list containing tuples of the form (m,b,True)            ##
##   each of these tuple correspond to some computation. You may want to remove      ##
##   some tuples in order to accelerate the computation (larger m's and b = True     ##
##   are more likely to take more time). You can also add inputs to the list is you  ##
##   want to verify the assumption on more inputs                                    ##
#######################################################################################


####################
## Imports needed ##
## and settings   ##
####################
import os, csv, time, argparse
load("functions.sage")

parser = argparse.ArgumentParser(
  prog="Testing Assumption 1 of ePrint 2024/441",
  description="It runs some numerical experiments to reproduce the data described in https://eprint.iacr.org/2024/441.pdf")
parser.add_argument('-i', '--input', default=None, help="file containing the list of tests to run (cf readme)")
parser.add_argument('-np', '--nbproc', type=int, default=3, help="number of processors for paralellism")
parser.add_argument('-rs', '--randomseed', choices=['random', 'fixed'], default='fixed', help="decides whether to use a fix random seed (set to 42) for the experiments, or not")
args = parser.parse_args()
    
## nbproc should be a integer, corresponding to the number of processors to run in parallel
nb_cpus = args.nbproc
print("The tests will be run in parallel using ", nb_cpus, " processors\n")

## randomseed is either 'fixed' (in this case the random seed of the experiments will be fixed to 42 for reproducibility) or not fixed
fixed_random_seed = (args.randomseed == 'fixed')
if fixed_random_seed:
  print("The tests will be run with a random seed fixed to 42 (same as in the article)\n")
  
## input should be the name of a csv file where each row is of the form "m random verbose", with 'm' an integer (the conductor of the field to test), 'random' is either True or False and decides whether we test with OK^2 (False) or with random modules (True), and 'verbose' is either True or False and decides whether the test is verbose (True) or not (False).
## the first row of the csv file should be "m,random,verbose" (csv file where each row is a dictionary)
input_file_name = args.input 
list_inputs = [] 

## If no argument input is given to the parser, then the list_input will be the one containing all the tests shown in the article
if input_file_name is None:
  print("The tests will be run on all inputs tested in the article\n")
  max_d = 45 ## for fields with degrees larger than max_d, does not perform the test with random modules
  for m in range(10,161):
    list_inputs += [(m,False,True)]
    d = euler_phi(m)/2
    if d <= max_d: ## we perform the test with random modules only if the degree of the field is <= max_d
      list_inputs += [(m,True,True)]
  for m in [178, 202, 171, 177, 254, 262, 274, 213, 185, 187, 249, 203, 173, 267, 181, 235, 191, 197, 275, 309, 265, 321, 247, 253, 339, 229, 233, 239, 251]:
    list_inputs += [(m,False,True)]
  ## we shuffle the list of input because the last entries in the list require the most memory usage, and running three in parallel may be too much on a small laptop. Shuffling the input makes it mush less likely that three memory consuming inputs are run at the same time
  if fixed_random_seed:
    set_random_seed(42)
  shuffle(list_inputs)
else:
  ## otherwise, load the list of tests from the csv file input_file_name
  print("The tests will be run on the list of inputs provided in file ",input_file_name)
  with open(input_file_name, newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
      list_inputs += [(ZZ(row['m']), row['random'] == "True", row['verbose'] == "True")]
  print("list_inputs = ", list_inputs,"\n")

######################################################
## Creating the folders for the results and figures ##
## if they do not already exist                     ##
######################################################
if not os.path.isdir("results"): ## test if the directory `results' already exists, and if not create it
  os.mkdir("results")
  print("Creating folder results...")
else:
  print("Folder results already exists, continuing...")
    
if not os.path.isdir("results/figures"): ## test if the directory `results/figures' already exists, and if not create it
  os.mkdir("results/figures")
  print("Creating folder results/figures...")
else:
  print("Folder results/figures already exists, continuing...")
    
####################################
## Creating the parallel function ##
## that will generate the data    ##
####################################
everything_went_well = True ## to check that no invalid operation occurs during the computation

@parallel(ncpus = nb_cpus)
def generate_data_parallel(m, random_modules, verbose = False):
  res = generate_data(m,random_modules,fixed_random_seed = fixed_random_seed, verbose = verbose)
  return res

###################################
##      Generating the data      ##
###################################

print("\nWe start generating the data for various number fields\n")
res = list(generate_data_parallel(list_inputs))
## sometimes the command above does not run all the tests from the list, so we check this here
tests_actually_performed = []
for x in res:
  if x[1] != 1:
    print("Something went wrong when performing the test", x[0][0],"\nHere is what we obtain for this test: ", x)
  else:
    tests_actually_performed += [x[0][0]]

all_done = True 
for z in list_inputs:
  if not z in tests_actually_performed:
    print("\nWarning, the tests were not run on the following input:", z)
    all_done = False

if all_done:
  print("\nAll the data was generated!\n")
else:
  print("\nThe data has been generated, but for some reasons some inputs are missing (see above), you may want to re-run the tests on those precise inputs.\n")

##################################
##      Loading the results     ##
##################################

## We store the results in two lists of lists (one big list for random modules and one big list for OK^2)
## Each sublist in the big lists corresponds to a number field and is of the form
##   [m,d,nb_tests, rho_K, everything_went_fine, l]
## where l is a list of triples, with one triple per module M generated. Each triple is of the form (P_empirical, P_expected, s)

res_modules = []
res_OK = []

for (m,random_modules,_) in tests_actually_performed:
  if random_modules:
    file_name = 'results/output_module_'+str(m)+'.csv'
  else:
    file_name = 'results/output_OK_'+str(m)+'.csv'
    
  ## reading corresponding csv file
  global_data = []
  module_data = []
  with open(file_name, newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    first_row = True
    for row in reader:
      if first_row:
        global_data = [ZZ(row['m']), ZZ(row['d']), ZZ(row['nb_tests']), RR(row['rho_K']), row['everything_went_well']=='True']
      first_row = False
      module_data += [(ZZ(row['P_empirical']), Reals(20)(row['P_expected']), RR(row['s']))]
      
  if random_modules:
    res_modules += [global_data + [module_data]]
  else:
    res_OK += [global_data + [module_data]]


###################################
##      Creating the plots       ##
###################################

print("Creating the plots...")

if fixed_random_seed:
  set_random_seed(42) ## to fix the random colors
(g_relative_OK, g_poly_OK) = print_results(res_OK)
if fixed_random_seed:
  set_random_seed(42) ## to fix the random colors
(g_relative_modules, g_poly_modules) = print_results(res_modules)
g_relative_sum = OK_among_modules(g_relative_OK, g_relative_modules)
g_poly_sum = OK_among_modules(g_poly_OK, g_poly_modules)

print("...done\n")

##################################
##   Saving plots in a file     ##
##################################
print("Saving the plots (this may take a few seconds)...")
p = g_relative_OK.plot()
p.save("results/figures/ratios_OK.png")
p = g_relative_modules.plot()
p.save("results/figures/ratios_modules.png")
p = g_relative_sum.plot()
p.save("results/figures/ratios_full.png")
p = g_poly_OK.plot()
p.save("results/figures/poly_OK.png")
p = g_poly_modules.plot()
p.save("results/figures/poly_modules.png")
p = g_poly_sum.plot()
p.save("results/figures/poly_full.png")
print("...done\n")
print("\nFigures generated, all done!\n")

